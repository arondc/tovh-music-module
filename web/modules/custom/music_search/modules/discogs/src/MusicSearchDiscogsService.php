<?php

namespace Drupal\music_search_discogs;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp;
use stdClass;

class MusicSearchDiscogsService
{
  protected ConfigFactoryInterface $configFactory;
  private mixed $config;

  public function __construct(ConfigFactoryInterface $config_factory){
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('music_search_discogs.api_key_form');
  }

  private function _discogs_api_get_query(string $uri): object {
    $token = $this->config->get('discogs_token');
    $options = array(
      'timeout' => 3,
      'headers' => array(
        'Accept' =>'Application/json',
        'Authorization' => "Discogs token=" . $token,
      ),
    );

    $client = new GuzzleHttp\Client();
    $search_results = $client->request('GET', $uri, $options);

    if (empty($search_results->error)) {
      $search_results = json_decode($search_results->getBody());
    }
    else {
      Drupal::messenger()->addStatus(t('The search request resulted in the following error: @error.', array(
        '@error' => $search_results->error,
      )));

      return $search_results->error;
    }

    return $search_results;
  }

  public function perform_search(string $query, string $type): stdClass {
    $discogs_search_uri = "https://api.discogs.com/database/search?q=";
    $discogs_results = $this->_discogs_api_get_query($discogs_search_uri . $query);
    # TODO: sort out the data we want
    $results = new stdClass();
    $results->albums = array();
    $results->artists = array();

    foreach ($discogs_results->results as $item) {
      if ($item->id) {
        switch ($item->type) {
          case "release":
            $results->albums[] = $this->_filter_data($item);
            break;

          case "artist":
            $results->artists[] = $this->_filter_data($item);
            break;
        }
      }
    }

    return $results;
  }

  private function _filter_data(object $item): object {
    $filteredData = new stdClass();
    $filteredData->id = $item->id;
    $filteredData->name = $item->title;

    return $filteredData;
  }

  public function get_artist_details(string $id): object {
    $discogs_artist_uri = "https://api.discogs.com/artists/";
    $artist = new stdClass();
    $discogs_artist = $this->_discogs_api_get_query($discogs_artist_uri . $id);

    $artist->name = $this->_clean_name($discogs_artist->name);
    $artist->id = $discogs_artist->id;
    $artist->images = array();
    $artist->description = $discogs_artist->profile;
    $artist->members = array();

    if(!empty($discogs_artist->images)){
      foreach ($discogs_artist->images as $key => $image) {
        $artist->images[] = $image->uri;
      }
    }

    if (isset($discogs_artist->members)) {
      foreach ($discogs_artist->members as $key => $member) {
        $artist->members[] = $member->id;
      }
    }

    return $artist;
  }

  public function search_for_artist(string $artist_name): object|NULL {
    $query_url = "https://api.discogs.com/database/search?type=artist&query=". $artist_name;
    $discogs_result = $this->_discogs_api_get_query($query_url);

    if (!empty($discogs_result->results)) {
      foreach ($discogs_result->results as $key => $artist) {
        if (strtolower($artist_name) == strtolower($this->_clean_name($artist->title))) {
          return $this->get_artist_details($artist->id);
        }
      }
      return NULL;
    }
    else {
      return NULL;
    }
  }

  public function search_for_album(string $album_name, array $artists): object|NULL {
    $query_url = "https://api.discogs.com/database/search?release_title=". $album_name;
    foreach ($artists as $key => $artist) {
      $query_url .= "&artist=". $artist->name;
    }

    $fullAlbum = NULL;
    $discogs_results = $this->_discogs_api_get_query($query_url);
    foreach ($discogs_results->results as $key => $result) {
      if($result->type == "master") {
        $fullAlbum = $this->get_master_details($result->id);
        break;
      }
    }

    $outputAlbum = new stdClass();
    if ($fullAlbum != NULL) {
      $releaseVersion = $this->get_release_details($fullAlbum->main_release);
      $outputAlbum->id = $fullAlbum->id;
      $outputAlbum->release_id = $fullAlbum->main_release;
      $outputAlbum->name = $fullAlbum->title;
      $outputAlbum->artists = array();
      $outputAlbum->images = array();
      $outputAlbum->labels = array();
      foreach ($fullAlbum->artists as $key => $artist) {
        $tmpArtist = new stdClass();
        $tmpArtist->id = $artist->id;
        $tmpArtist->name = $this->_clean_name($artist->name);

        $outputAlbum->artists[] = $tmpArtist;
      }

      foreach ($fullAlbum->images as $key => $image) {
        $outputAlbum->images[] = $image->uri;
      }

      foreach ($releaseVersion->labels as $key => $label) {
        $tmpLabel = new stdClass();
        $tmpLabel->name = $label->name;
        $tmpLabel->logo = $label->thumbnail_url;
        $tmpLabel->id = $label->id;
        $outputAlbum->labels[] = $tmpLabel;
      }

      return $outputAlbum;
    }
    else {
      return null;
    }
  }

  private function get_master_details(string $id): object {
    $discogs_master_uri = "https://api.discogs.com/masters/";
    return $this->_discogs_api_get_query($discogs_master_uri . $id);
  }

  private function get_release_details(int $id): object {
    $discogs_release_uri = "https://api.discogs.com/releases/";
    return $this->_discogs_api_get_query($discogs_release_uri . $id);
  }

  public function get_label_details(string $id): object {
    $discogs_labels_uri = "https://api.discogs.com/labels/";
    $label = new stdClass();
    $discogs_label = $this->_discogs_api_get_query($discogs_labels_uri . $id);
    $label->id = $discogs_label->id;
    $label->name = $discogs_label->name;
    $label->description = $discogs_label->profile;
    $label->images = array();

    foreach ($discogs_label->images as $key => $image) {
      $label->images[] = $image->uri;
    }

    return $label;
  }

  private function _clean_name(string $name): string {
    if (str_contains($name, " (")) {
      $splitName = explode(" (", $name);
      return $splitName[0];
    }
    else {
      return $name;
    }
  }
}
