<?php

namespace Drupal\music_search_discogs\Forms;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class MusicSearchDiscogsConfigurationForm extends ConfigFormBase {
  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames(): array {
    return ['music_search_discogs.api_key_form'];
  }

  /**
   * @inheritDoc
   */
  public function getFormId(): string {
    return 'music_search_discogs_configuration_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('music_search_discogs.api_key_form');

    $form['discogs_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Discogs API token'),
      '#description' => $this->t('Put your personal discogs token here'),
      '#default_value' => $config->get('discogs_token')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('music_search_discogs.api_key_form')
      ->set('discogs_token', $form_state->getValue('discogs_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}