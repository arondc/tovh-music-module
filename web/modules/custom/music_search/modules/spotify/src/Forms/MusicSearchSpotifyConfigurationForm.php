<?php

namespace Drupal\music_search_spotify\Forms;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class MusicSearchSpotifyConfigurationForm extends ConfigFormBase {
  /**
   * @inerhitDoc
   */
  protected function getEditableConfigNames(): array {
    return ['music_search_spotify.api_key_form'];
  }

  /**
   * @inheritDoc
   */
  public function getFormId(): string {
    return 'music_search_spotify_configuration_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('music_search_spotify.api_key_form');

    $form['spotify_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spotify Client ID'),
      '#description' => $this->t('Put the spotify client id here.'),
      '#default_value' => $config->get('spotify_client_id')
    ];

    $form['spotify_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spotify Client Secret'),
      '#description' => $this->t('Put the spotify client secret here.'),
      '#default_value' => $config->get('spotify_client_secret')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('music_search_spotify.api_key_form')
      ->set('spotify_client_id', $form_state->getValue('spotify_client_id'))
      ->set('spotify_client_secret', $form_state->getValue('spotify_client_secret'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}