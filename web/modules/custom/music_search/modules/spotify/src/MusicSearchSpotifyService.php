<?php

namespace Drupal\music_search_spotify;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp;
use stdClass;

class MusicSearchSpotifyService
{
  /**
   * @var ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;
  private mixed $config;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('music_search_spotify.api_key_form');
  }

  /**
   * Sends a GET query to Spotify for specific URL
   *
   * @param $uri string
   *   The fully generated search string
   * @return object
   *   Returns a stdClass with the search results or an error message
   * @throws GuzzleHttp\Exception\GuzzleException
   */
  function _spotify_api_get_query(string $uri): object
  {
    $token = $this->_spotify_api_get_auth_token();
    $token = json_decode($token);
    $options = array(
      'timeout' => 3,
      'headers' => array(
        'Accept' => 'application/json',
        'Authorization' => "Bearer " . $token->access_token,
      ),
    );

    $client = new GuzzleHttp\Client();
    $search_results = $client->request('GET', $uri, $options);

    if (empty($search_results->error)) {
      $search_results = json_decode($search_results->getBody());
    }
    else {
      Drupal::messenger()->addStatus(t('The search request resulted in the following error: @error.', array(
        '@error' => $search_results->error,
      )));

      return $search_results->error;
    }

    return $search_results;
  }

  /**
   * Gets Auth token from the Spotify API
   */
  function _spotify_api_get_auth_token(): bool|string
  {
    $SPOTIFY_CLIENT_ID = $this->config->get('spotify_client_id');
    $SPOTIFY_CLIENT_SECRET = $this->config->get('spotify_client_secret');

    $connection_string = "https://accounts.spotify.com/api/token";
    $key = base64_encode($SPOTIFY_CLIENT_ID . ':' . $SPOTIFY_CLIENT_SECRET);
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $connection_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Authorization: Basic " . $key;
    $headers[] = "Content-Type: application/x-www-form-urlencoded";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);

    curl_close($ch);
    return $result;
  }

  /**
   * @throws GuzzleHttp\Exception\GuzzleException
   */
  function perform_search($query): stdClass
  {
    $results = new stdClass();
    $results->albums = array();
    $results->artists = array();
    $results->tracks = array();

    $spotify_uri = 'https://api.spotify.com/v1/search?type=album,artist,track&limit=3&q=' . $query;
    $spotify_results = $this->_spotify_api_get_query($spotify_uri);

    for($i = 0; $i < $spotify_results->albums->limit; $i++) {
      $tmpAlbum = new stdClass();
      $tmpAlbum->id = $spotify_results->albums->items[$i]->id;
      $tmpAlbum->name = $spotify_results->albums->items[$i]->name;
      $results->albums[$i] = $tmpAlbum;

      $tmpArtist = new stdClass();
      $tmpArtist->id = $spotify_results->artists->items[$i]->id;
      $tmpArtist->name = $spotify_results->artists->items[$i]->name;
      $results->artists[$i] = $tmpArtist;

      $tmpTrack = new stdClass();
      $tmpTrack->id = $spotify_results->tracks->items[$i]->id;
      $tmpTrack->name = $spotify_results->tracks->items[$i]->name;
      $results->tracks[$i] = $tmpTrack;
    }

    return $results;
  }

  /**
   * @throws GuzzleHttp\Exception\GuzzleException
   */
  function get_album_details($album_id): object
  {
    $uri = 'https://api.spotify.com/v1/albums/' . $album_id;
    return $this->_spotify_api_get_query($uri);
  }

  public function get_track_details(string $id): object {
    $uri = 'https://api.spotify.com/v1/tracks/' . $id;
    $track = $this->_spotify_api_get_query($uri);
    $filtered_track = new stdClass();
    $filtered_track->id = $track->id;
    $filtered_track->name = $track->name;
    $filtered_track->uri = $track->external_urls->spotify;
    $seconds = ($track->duration_ms / 1000) % 60;
    if($seconds < 10){$seconds = '0' . $seconds;}
    $filtered_track->length = strval((int)(($track->duration_ms / 1000) / 60)) . ':' . strval($seconds);
    return $filtered_track;
  }

  public function get_artist_details(string $id): object {
    $uri = 'https://api.spotify.com/v1/artists/' . $id;
    $artist = $this->_spotify_api_get_query($uri);
    $filtered_artist = new stdClass();
    $filtered_artist->id = $artist->id;
    $filtered_artist->images = array();
    $filtered_artist->name = $artist->name;

    foreach ($artist->images as $key => $image) {
      $filtered_artist->images[] = $image->url;
    }

    return $filtered_artist;
  }
}
