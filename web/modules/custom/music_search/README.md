# TOVH Music search module

## Description
The TOVH Music search module is a one of a kind ;) search module that allows you to add the ability to search multiple music data api's to your drupal 9.2+ site. The module can currently search the spotify and discogs api's.

## Functionality
### Searching
The default functionality of this module is to allow the user to search for data with a search form located under
```
/music/search
```
There you can type in your search string, the form will give you autocomplete results in a dropdown menu from the text box. You can then select the result you want, if you are satisfied you have selected the correct result you may click the "next" button.

### Creating
After searching the site will take you to a form where you can select what data from the api's you want to add to your new node. After the selection the system will auto create any selected nodes that need to be created and link them to your node. For an example if you create an Album and select the Producer and Artist fields on the editing page, the system will first check if those exist in the local system if they do they are linked to the new node if they don't then the system will query the api's and auto create the nodes to be able to link everything toghether.

### What if the node already exists?
If the item you are searching for already exists, the system will update the existing node for that item with the selected information instead of creating a new one

### Publishing status
By default the module creates nodes as unpublished, so an editor can review the information before manually publishing the node.

### What data is auto created?
Currently we support auto creating and linking the following content-types
- Artists
- Producers/Publishers
- Tracks

## Installation
The module requires two sub modules to be installed those are the "Music Search Spotify" and "Music Search Discogs" sub modules.

To install the module you need to move the "music_search" folder that contains this module and it's submodules to the modules folder of your drupal 9.2+ installation. Then you can enable the "Music Search Module" in your drupal GUI, drupal will then tell you that this module requires it's sub modules and if you want to install them, select "yes".

## Configuration
Before you can start creating items you need to configure the api keys for the api's you want to be able to access
### Spofity api configuration
To configure the spotify api go to
````
/admin/spotify/config
````
There you can enter your spotify client id and client secret


#### How do I get the spotify client id and secret?
1. Go to the [spotify developer site](https://developer.spotify.com/dashboard/login) and log in with your spotify account.
2. Create a new app in the menu spotify gives you.
    * Give your app a descriptive name for example "yoursite - Music search"
    * Give it a description
    * Aggree to the spotify terms and conditions
    * Click the "Create" button.
3. Once your app has been created you will be presented with a page displaying your client id, copy it over you the spotify configuration form on your drupal site
4. Go back to the spotify app page and click the "Show client secret" button, copy the secret key it shows you over to the spotify configuration form on your drupal site
5. Hit save and you're done.


### Discogs api configuration
To configure the discogs api go to
````
/admin/discogs/config
````
There you can enter your discogs api token

#### How do I get the discogs api token?
1. Go to the [Discogs api](https://accounts.discogs.com/login) and log in or create an account
2. Go to the [Developer settings page](https://www.discogs.com/settings/developers)
3. Click the "Generate New Token" button
4. Copy your token over to the discogs configuration form on your drupal site.

