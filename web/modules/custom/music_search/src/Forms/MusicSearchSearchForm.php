<?php
namespace Drupal\music_search\Forms;

use Drupal;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\music_search\MusicSearchAutoCreationService;
use Drupal\music_search_discogs\MusicSearchDiscogsService;
use Drupal\music_search_spotify\MusicSearchSpotifyService;
use Drupal\music_search\MusicSearchCreationService;
use stdClass;

// TODO: Implement validateForm for all custom forms
class MusicSearchSearchForm extends FormBase {
  /**
   * @inheritDoc
   */
  public function getFormId(): string {
    return 'music_search_search_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    if ($form_state->has('page') && $form_state->get('page') == 2) {
      return self::form_page_two($form, $form_state);
    }

    $form_state->set('page', 1);

    $form['search_string'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search string'),
      '#description' => $this->t('Keyword to search for.'),
      '#default_value' => '',
      '#autocomplete_route_name' => 'music_search.autocomplete',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Next'),
      '#submit' => ['::submit_page_one'],
    ];

    return $form;
  }

  private function _extract_id(string $value): array {
    $output = [];

    $value_array = explode(' ', $value);
    $value_last_elem = end($value_array);

    $output['id'] = trim($value_last_elem, '()');
    array_pop($value_array);
    $output['value'] = implode(' ', $value_array);

    return $output;
  }

  /**
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submit_page_one(array &$form, FormStateInterface $form_state) {
    $data = $form_state->getValue('search_string');

    $search_id = $this->_extract_id($data)['id'];

    $search_type = NULL;

    $res = Drupal::state()->get('autocomplete_result');
    for($i = 0; $i < count($res); $i++) {
      if($res[$i]['id'] == $search_id) {
        $search_type = $res[$i]['type'];
        break;
      }
    }

    $form_state
      ->set('page_values', [
        'search_string' => $form_state->getValue('search_string'),
        'search_id' => $search_id,
        'search_type' => $search_type,
      ])
      ->set('page', 2)
      ->setRebuild(TRUE);
  }

  private function _create_table(array &$form, array $data) {
    $table_header = [
      'label' => $this->t('Label'),
      'data' => $this->t('Data'),
    ];

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $table_header,
      '#options' => $data,
      '#empty' => $this->t('No data'),
    ];
  }

  private function add_table_row(array &$table_data, $field_name, $data, $label, $data_override = NULL) {
    $field_data = $data;

    if (!empty($data)) {
      if ($data_override) {
        $field_data = $data_override;
      }

      if (gettype($field_data) == 'array') {
        $field_data = implode(', ', $field_data);
      }

      $table_data[$field_name] = [
        'label' => $label,
        'data' => $field_data,
      ];
    }
  }

  private function _spotify_album_form(string $id, array &$table_data): object|NULL {
    $spotify_info = Drupal::getContainer()->get(MusicSearchSpotifyService::class)->get_album_details($id);

    $image_data = new FormattableMarkup('<img src="@url" width="200px">', ['@url' => $spotify_info->images[0]->url]);

    // Prepare artist data
    $artists = [];
    if (!empty($spotify_info->artists)) {
      foreach ($spotify_info->artists as $artist) {
        $artists[] = $artist->name;
      }
    }

    $tracks = [];
    if (!empty($spotify_info->tracks->items)) {
      foreach ($spotify_info->tracks->items as $track) {
        $tracks[] = $track->name . ' (' . $track->id . ')';
      }
    }

    $this->add_table_row($table_data, 'spotify_album_name', $spotify_info->name, 'Album Name (Spotify)');
    $this->add_table_row($table_data, 'spotify_album_image', $spotify_info->images[0], 'Album Image (Spotify)', $image_data);
    $this->add_table_row($table_data, 'spotify_album_artists', $artists, 'Album Artist (Spotify)');
    $this->add_table_row($table_data, 'spotify_track_list', $tracks, 'Track List (Spotify)');

    return $spotify_info;
  }

  private function _discogs_album_form(object $album_info, array &$table_data, FormStateInterface &$form_state): object|NULL {
    $discogs_album_info = Drupal::getContainer()->get(MusicSearchDiscogsService::class)->search_for_album($album_info->name, $album_info->artists);

    if (!empty($discogs_album_info)) {
      $num_images = 0;
      if (isset($discogs_album_info->images)) {
        $num_images = count($discogs_album_info->images);
      }

      $form_state->set('num_discogs_images', $num_images);

      // Prepare artists
      $artists = [];

      if (!empty($discogs_album_info->artists)) {
        foreach ($discogs_album_info->artists as $artist) {
          $artists[] = $artist->name . ' (' . $artist->id . ')';
        }
      }

      // Prepare labels
      $labels = [];

      if (!empty($discogs_album_info->labels)) {
        foreach ($discogs_album_info->labels as $label) {
          $labels[] = $label->name . ' (' . $label->id . ')';
        }
      }

      $this->add_table_row($table_data, 'discogs_album_name', $discogs_album_info->name, 'Album Name (Discogs)');
      $this->add_table_row($table_data, 'discogs_album_artists', $artists, 'Album Artists (Discogs)');

      // Add images
      if (!empty($discogs_album_info->images)) {
        foreach ($discogs_album_info->images as $key => $image) {
          $formatted_image = new FormattableMarkup('<img src="@url" width="200px">', ['@url' => $image]);
          $this->add_table_row($table_data, 'discogs_album_image_' . $key, $image, 'Album Image (Discogs)', $formatted_image);
        }
      }

      $this->add_table_row($table_data, 'discogs_album_producers', $labels, 'Album Producers (Discogs)');

      return $discogs_album_info;
    }
    else {
      return NULL;
    }
  }

  private function _spotify_artist_form(string $id, array &$table_data): object|NULL {
    $spotify_info = Drupal::getContainer()->get(MusicSearchSpotifyService::class)->get_artist_details($id);

    $image_data = new FormattableMarkup('<img src="@url" width="200px">', ['@url' => $spotify_info->images[0]]);

    $this->add_table_row($table_data, 'spotify_artist_name', $spotify_info->name, 'Artist Name (Spotify)');
    $this->add_table_row($table_data, 'spotify_artist_image', $spotify_info->images[0], 'Artist Image (Spotify)', $image_data);

    return $spotify_info;
  }

  private function _discogs_artist_form(object $artist_info, array &$table_data, FormStateInterface &$form_state): object|NULL {
    $discogs_info = Drupal::getContainer()->get(MusicSearchDiscogsService::class)->search_for_artist($artist_info->name);

    if (!empty($discogs_info)) {
      $num_images = 0;
      if (isset($discogs_info->images)) {
        $num_images = count($discogs_info->images);
      }

      $form_state->set('num_discogs_images', $num_images);
      $this->add_table_row($table_data, 'discogs_artist_name', $discogs_info->name, 'Artist Name (Discogs)');
      $this->add_table_row($table_data, 'discogs_artist_description', $discogs_info->description, 'Artist Description (Discogs)');

      // Add images
      if (!empty($discogs_info->images)) {
        foreach ($discogs_info->images as $key => $image) {
          $formatted_image = new FormattableMarkup('<img src="@url" width="200px">', ['@url' => $image]);
          $this->add_table_row($table_data, 'discogs_artist_image_' . $key, $image, 'Artist Image (Discogs)', $formatted_image);
        }
      }

      return $discogs_info;
    }
    else {
      return NULL;
    }
  }

  private function _spotify_track_form(string $id, array &$table_data): object|NULL {
    $spotify_info = Drupal::getContainer()->get(MusicSearchSpotifyService::class)->get_track_details($id);

    $this->add_table_row($table_data, 'spotify_track_name', $spotify_info->name, 'Track Name (Spotify)');
    $this->add_table_row($table_data, 'spotify_track_length', $spotify_info->length, 'Track Length (Spotify)');
    $this->add_table_row($table_data, 'spotify_track_uri', $spotify_info->uri, 'Track URI (Spotify)');

    return $spotify_info;
  }

  private function _build_album_form(array &$form, FormStateInterface $form_state, array &$table_data, string $id): bool {
    if (empty($id)) {
      Drupal::messenger()->addError('Album ID is empty');
      return FALSE;
    }

    $spotify_info = $this->_spotify_album_form($id, $table_data);
    $discogs_info = $this->_discogs_album_form($spotify_info, $table_data, $form_state);

    $form['spotify_album_id'] = [
      '#type' => 'hidden',
      '#value' => $spotify_info->id,
      '#disabled' => TRUE,
    ];

    if (!empty($discogs_info)) {
      $form['discogs_album_id'] = [
        '#type' => 'hidden',
        '#value' => $discogs_info->id,
        '#disabled' => TRUE,
      ];
    }

    return TRUE;
  }

  private function _build_artist_form(array &$form, FormStateInterface $form_state, array &$table_data, string $id): bool {
    if (empty($id)) {
      Drupal::messenger()->addError('Artist ID is empty');
      return FALSE;
    }

    $spotify_info = $this->_spotify_artist_form($id, $table_data);
    $discogs_info = $this->_discogs_artist_form($spotify_info, $table_data, $form_state);

    $form['spotify_artist_id'] = [
      '#type' => 'hidden',
      '#value' => $spotify_info->id,
      '#disabled' => TRUE,
    ];

    if (!empty($discogs_info)) {
      $form['discogs_artist_id'] = [
        '#type' => 'hidden',
        '#value' => $discogs_info->id,
        '#disabled' => TRUE,
      ];
    }

    return TRUE;
  }

  private function _build_track_form(array &$form, FormStateInterface $form_state, array &$table_data, string $id): bool {
    if (empty($id)) {
      Drupal::messenger()->addError('Track ID is empty');
      return FALSE;
    }

    $spotify_info = $this->_spotify_track_form($id, $table_data);

    $form['spotify_track_id'] = [
      '#type' => 'hidden',
      '#value' => $spotify_info->id,
      '#disabled' => TRUE,
    ];

    return TRUE;
  }

  /**
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   * @return array
   *   The render array defining the elements of the form.
   */
  public function form_page_two(array &$form, FormStateInterface $form_state): array {
    $submit_type = $form_state->get('page_values')['search_type'];
    $id = $form_state->get('page_values')['search_id'];
    $table_data = [];

    switch ($submit_type) {
      case 'albums':
        $this->_build_album_form($form, $form_state, $table_data, $id);
        break;

      case 'artists':
        $this->_build_artist_form($form, $form_state, $table_data, $id);
        break;

      case 'tracks':
        $this->_build_track_form($form, $form_state, $table_data, $id);
        break;
    }

    // Display the data
    $this->_create_table($form, $table_data);

    $form['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function page_two_back(array &$form, FormStateInterface $form_state): void {
    $form_state
      ->setValues($form_state->get('page_values'))
      ->set('page', 1)
      ->setRebuild(TRUE);
  }

  private function _get_table_data(string $key, FormStateInterface $form_state): string|NULL {
    if (!empty($form_state->getCompleteForm()['table']['#value'][$key])) {
      return $form_state->getCompleteForm()['table']['#options'][$key]['data'];
    }

    return NULL;
  }

  private function _prepare_multi_value_data(string $data): array {
    $data = explode('), ', $data);
    $output = [];

    foreach ($data as $tmp) {
      $output[] = $this->_extract_id($tmp);
    }

    return $output;
  }

  private function _get_image_url_from_html(string $html): string {
    return explode('"', $html)[1];
  }

  private function _submit_album(array &$form, FormStateInterface $form_state): int {
    // Get Spotify data
    $s_album_id = $form_state->getValue('spotify_album_id');
    $s_album_name = $form_state->getCompleteForm()['table']['#options']['spotify_album_name']['data'];
    $s_album_image_html = $this->_get_table_data('spotify_album_image', $form_state);
    $s_album_artists = $this->_get_table_data('spotify_album_artists', $form_state);
    $s_track_list = $this->_get_table_data('spotify_track_list', $form_state);

    // Get Discogs data
    $d_album_id = $form_state->getValue('discogs_album_id');
    $d_album_name = $this->_get_table_data('discogs_album_name', $form_state);
    $d_album_artists = $this->_get_table_data('discogs_album_artists', $form_state);
    $d_album_publishers = $this->_get_table_data('discogs_album_producers', $form_state);

    $auto_create = Drupal::getContainer()->get(MusicSearchAutoCreationService::class);

    // Prioritize duplicate data
    $album_name = NULL;
    $album_artists = NULL;
    $album_images = [];

    $album_name = $s_album_name;
    if (!empty($d_album_name) && $form_state->getValues()['table']['spotify_album_name'] === 1) {
      $album_name = $d_album_name;
    }

    if (!empty($d_album_artists)) {
      $album_artists = $d_album_artists;
    }
    elseif (!empty($s_album_artists)) {
      $album_artists = $s_album_artists;
    }

    // Prepare values
    if (!empty($s_album_image_html)) {
      $s_album_image_link = $this->_get_image_url_from_html($s_album_image_html);
    }

    if (!empty($album_artists)) {
      $artists = $this->_prepare_multi_value_data($album_artists);
    }

    if (!empty($s_track_list)) {
      $s_tracks = $this->_prepare_multi_value_data($s_track_list);
    }

    if (!empty($d_album_publishers)) {
      $d_publishers = $this->_prepare_multi_value_data($d_album_publishers);
    }

    if (!empty($s_album_image_link)) {
      $album_images[] = $auto_create->add_media_image($s_album_image_link, $album_name, 'album_artwork')->id();
    }

    for ($i = 0; $i < $form_state->get('num_discogs_images'); $i++) {
      $image_html = $this->_get_table_data('discogs_album_image_' . $i, $form_state);
      if (!empty($image_html)) {
        $image_url = $this->_get_image_url_from_html($image_html);
        $album_images[] = $auto_create->add_media_image($image_url, $album_name, 'album_artwork')->id();
      }
    }

    $publisher_ids = [];
    $artist_ids = [];

    if (!empty($d_album_id)) {
      if (!empty($d_publishers)) {
        foreach ($d_publishers as $publisher) {
          $publisher_ids[] = $auto_create->create_or_link_publisher($publisher['id']);
        }
      }

      if (!empty($artists)) {
        foreach ($artists as $artist) {
          $artist_ids[] = $auto_create->create_or_link_artist($artist['id']);
        }
      }
    }

    $track_ids = [];
    if (!empty($s_tracks)) {
      foreach ($s_tracks as $track) {
        $track_ids[] = $auto_create->create_or_link_track($track['id']);
      }
    }

    $album_data = new stdClass();
    $album_data->name = $album_name;
    $album_data->artists = $artist_ids;
    $album_data->discogs_id = $d_album_id;
    $album_data->spotify_id = $s_album_id;
    $album_data->images = $album_images;
    $album_data->publishers = $publisher_ids;
    $album_data->track_list = $track_ids;

    return Drupal::getContainer()->get(MusicSearchCreationService::class)->create_or_edit_album($album_data);
  }

  private function _submit_artist(array &$form, FormStateInterface $form_state): int {
    // Get Spotify data
    $s_id = $form_state->getValue('spotify_artist_id');
    $s_name = $form_state->getCompleteForm()['table']['#options']['spotify_artist_name']['data'];
    $s_image_html = $this->_get_table_data('spotify_artist_image', $form_state);

    // Get Discogs data
    $d_id = $form_state->getValue('discogs_artist_id');
    $d_name = $this->_get_table_data('discogs_artist_name', $form_state);
    $d_description = $this->_get_table_data('discogs_artist_description', $form_state);

    $auto_create = Drupal::getContainer()->get(MusicSearchAutoCreationService::class);

    // Prioritize duplicate data
    $name = NULL;
    $images = [];

    $name = $s_name;
    if (!empty($d_name) && $form_state->getValues()['table']['spotify_artist_name'] === 1) {
      $name = $d_name;
    }

    // Prepare values
    if (!empty($s_image_html)) {
      $s_image_link = $this->_get_image_url_from_html($s_image_html);
    }

    if (!empty($s_image_link)) {
      $images[] = $auto_create->add_media_image($s_image_link, $name, 'artist_image')->id();
    }

    for ($i = 0; $i < $form_state->get('num_discogs_images'); $i++) {
      $image_html = $this->_get_table_data('discogs_artist_image_' . $i, $form_state);
      if (!empty($image_html)) {
        $image_url = $this->_get_image_url_from_html($image_html);
        $images[] = $auto_create->add_media_image($image_url, $name, 'artist_image')->id();
      }
    }

    $data = new stdClass();
    $data->name = $name;
    $data->description = $d_description;
    $data->discogs_id = $d_id;
    $data->spotify_id = $s_id;
    $data->images = $images;

    return Drupal::getContainer()->get(MusicSearchCreationService::class)->create_or_edit_artist($data);
  }

  private function _submit_track(array &$form, FormStateInterface $form_state): int {
    // Get Spotify data
    $s_id = $form_state->getValue('spotify_track_id');
    $s_name = $form_state->getCompleteForm()['table']['#options']['spotify_track_name']['data'];
    $s_length = $this->_get_table_data('spotify_track_length', $form_state);
    $s_uri = $this->_get_table_data('spotify_track_uri', $form_state);

    $data = new stdClass();
    $data->spotify_id = $s_id;
    $data->name = $s_name;
    $data->length = $s_length;
    $data->uri = $s_uri;

    return Drupal::getContainer()->get(MusicSearchCreationService::class)->create_or_edit_track($data);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state): FormStateInterface {
    $submit_type = $form_state->get('page_values')['search_type'];
    $node_id = NULL;

    switch ($submit_type) {
      case 'albums':
        $node_id = $this->_submit_album($form, $form_state);
        break;

      case 'artists':
        $node_id = $this->_submit_artist($form, $form_state);
        break;

      case 'tracks':
        $node_id = $this->_submit_track($form, $form_state);
        break;
    }

    $url = Url::fromRoute('entity.node.canonical', ['node' => $node_id]);
    return $form_state->setRedirectUrl($url);
  }
}
