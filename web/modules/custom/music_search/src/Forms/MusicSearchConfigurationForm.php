<?php

namespace Drupal\music_search\Forms;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class MusicSearchConfigurationForm extends ConfigFormBase {
  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames(): array {
    return ['music_search.api_key_form'];
  }

  /**
   * @inheritDoc
   */
  public function getFormId(): string {
    return 'music_search_configuration_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }
}
