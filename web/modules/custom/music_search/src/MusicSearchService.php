<?php

namespace Drupal\music_search;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\music_search_discogs\MusicSearchDiscogsService;
use Drupal\music_search_spotify\MusicSearchSpotifyService;
use GuzzleHttp;
use stdClass;

class MusicSearchService
{
  /**
   * @var ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;
  private mixed $config;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('music_search.api_key_form');
  }

  private function _combine_results(array $spotify, array $discogs): array {
    $discogs_result_copy = $discogs;
    $spotify_result_copy = $spotify;

    $combined = array();
    foreach ($spotify as $s_id => $s_value) {
      foreach ($discogs as $d_id => $d_value) {
        if ($s_value->name == $d_value->name) {
          $res = new stdClass();
          $res->s_id = $s_id;
          $res->d_id = $d_id;
          $res->name = $s_value->name;
          $combined[] = $res;
          unset($discogs_result_copy[$d_id]);
          unset($spotify_result_copy[$s_id]);
          break;
        }
      }
    }

    return $combined;
  }

  /**
   * @throws GuzzleHttp\Exception\GuzzleException
   */
  function perform_search(string $query): stdClass {
    // TODO: Change to dependecy injection
    $discogs_result = Drupal::getContainer()->get(MusicSearchDiscogsService::class)->perform_search($query);
    $spotify_result = Drupal::getContainer()->get(MusicSearchSpotifyService::class)->perform_search($query);

    $combined_result = new stdClass();
    $combined_result->albums = $this->_combine_results($spotify_result->albums, $discogs_result->albums);
    $combined_result->artists = $this->_combine_results($spotify_result->artists, $discogs_result->artists);

    return $combined_result;
  }
}
