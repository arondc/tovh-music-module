<?php

namespace Drupal\music_search;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp;
use stdClass;

class MusicSearchCreationService {
	public function create_or_edit_album(object $data) {
    $conditions = new stdClass();
    $conditions->field_name = ['field_spotify_id', 'field_discogs_id'];
    $conditions->value = [$data->spotify_id, $data->discogs_id];

    $node_id = $this->_node_exists($conditions, 'album');
    $node = NULL;

    $values = [
      'type' => 'album',
      'title' => $data->name,
      'field_album_image' => $data->images,
      'field_publisher' => $data->publishers,
      'field_artist' => $data->artists,
      'field_song_list' => $data->track_list,
      'field_spotify_id' => $data->spotify_id,
      'field_discogs_id' => $data->discogs_id,
    ];

    if ($node_id !== -1) {
      $node = Drupal::entityTypeManager()->getStorage('node')->load($node_id);

      foreach ($values as $field => $value) {
        if ($field !== 'type') {
          $node->set($field, $value);
        }
      }
    }
    else {
      $node = Drupal::entityTypeManager()->getStorage('node')
        ->create($values);
    }

    $node->save();
    return $node->id();
	}

  public function create_or_edit_artist(object $data) {
    $conditions = new stdClass();
    $conditions->field_name = ['field_spotify_id', 'field_discogs_id'];
    $conditions->value = [$data->spotify_id, $data->discogs_id];

    $node_id = $this->_node_exists($conditions, 'artist');
    $node = NULL;

    $values = [
      'type' => 'artist',
      'title' => $data->name,
      'field_artist_image' => $data->images,
      'field_spotify_id' => $data->spotify_id,
      'field_discogs_id' => $data->discogs_id,
      'body' => [
        'value' => $data->description,
      ],
    ];

    if ($node_id !== -1) {
      $node = Drupal::entityTypeManager()->getStorage('node')->load($node_id);

      foreach ($values as $field => $value) {
        if ($field !== 'type') {
          $node->set($field, $value);
        }
      }
    }
    else {
      $node = Drupal::entityTypeManager()->getStorage('node')
        ->create($values);
    }

    $node->save();
    return $node->id();
	}

  public function create_or_edit_track(object $data) {
    $conditions = new stdClass();
    $conditions->field_name = ['field_spotify_id'];
    $conditions->value = [$data->spotify_id];

    $node_id = $this->_node_exists($conditions, 'song');
    $node = NULL;

    $values = [
      'type' => 'song',
      'title' => $data->name,
      'field_spotify_id' => $data->spotify_id,
      'field_spotify_uri' => $data->uri,
      'field_song_length' => $data->length,
    ];

    if ($node_id !== -1) {
      $node = Drupal::entityTypeManager()->getStorage('node')->load($node_id);

      foreach ($values as $field => $value) {
        if ($field !== 'type') {
          $node->set($field, $value);
        }
      }
    }
    else {
      $node = Drupal::entityTypeManager()->getStorage('node')
        ->create($values);
    }

    $node->save();
    return $node->id();
	}

  private function _node_exists(object $conditions, string $type): int {
    $query = Drupal::entityQuery('node')
      ->condition('type', $type);

    $group = $query
      ->orConditionGroup();

    foreach ($conditions->field_name as $key => $value) {
      if (!empty($conditions->value[$key])) {
        $group->condition($conditions->field_name[$key], $conditions->value[$key]);
      }
    }

    $node_ids = array_values($query->condition($group)->execute());

    if (count($node_ids) != 0) {
      return $node_ids[0];
    }
    else {
      return -1; // Item does not exists
    }
  }
}
