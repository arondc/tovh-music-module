<?php

namespace Drupal\music_search;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\media\Entity\Media;
use Drupal\music_search_discogs\MusicSearchDiscogsService;
use Drupal\music_search_spotify\MusicSearchSpotifyService;

class MusicSearchAutoCreationService {
  /**
   * @var ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;
  private mixed $config;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  public function create_or_link_publisher(int $discogs_id): int {
    if (($id = $this->_node_exists($discogs_id, 'publisher')) != -1){
      return $id;
    }
    else {
      $publisher = Drupal::getContainer()->get(MusicSearchDiscogsService::class)->get_label_details(strval($discogs_id));
      // create media before setting the node values
      $logo = $this->add_media_image($publisher->images[0], $publisher->name, 'publisher_logo');
      $publisher_values = [
        'type' => 'publisher',
        'title' => $publisher->name,
        'field_discogs_id' => $publisher->id,
        'body' => [
          'value' => $publisher->description
        ],
        'field_logo' => $logo->id(),
      ];

      return $this->_create_node($publisher_values);
    }
  }

  public function create_or_link_artist(int $discogs_id): int {
    if (($id = $this->_node_exists($discogs_id, 'artist')) != -1){
      return $id;
    }
    else {
      $artist = Drupal::getContainer()->get(MusicSearchDiscogsService::class)->get_artist_details(strval($discogs_id));
      // create media items for all images
      $images = [];
      foreach ($artist->images as $key => $image) {
        $images[] = $this->add_media_image($image, $artist->name, 'artist_image');
      }

      if(!empty($artist->members)){
        $tmpMemberIds = [];
        foreach ($artist->members as $key => $member_id) {
          $tmpMemberIds[] = $this->create_or_link_artist($member_id);
        }
        $artist->members = $tmpMemberIds;
        $artist->type = 6;
      }
      else {
        $artist->type = 5;
      }

      $artist_values = [
        'type' => 'artist',
        'title' => $artist->name,
        'field_discogs_id' => $artist->id,
        'body' => [
          'value' => $artist->description,
        ],
        'field_artist_image' => $images,
        'field_band_members' => $artist->members,
        'field_artist_type' => [
          ['target_id' => $artist->type],
        ],
      ];

      return $this->_create_node($artist_values);
    }
  }

  public function create_or_link_track(string $spotify_id): int {
    if (($id = $this->_node_exists($spotify_id, 'song', 'field_spotify_id')) != -1){
      return $id;
    }
    else {
      $track = Drupal::getContainer()->get(MusicSearchSpotifyService::class)->get_track_details(strval($spotify_id));
      $track_values = [
        'type' => 'song',
        'title' => $track->name,
        'field_spotify_id' => $track->id,
        'field_spotify_uri' => $track->uri,
        'field_song_length' => $track->length,
      ];

      return $this->_create_node($track_values);
    }
  }

  private function _create_node(array $node_values): int{
    $node = Drupal::entityTypeManager()->getStorage('node')->create($node_values);
    $node->save();
    return $node->id();
  }

  private function _exists_artist(int $id): int {
    return $this->_node_exists($id, 'artist');
  }

  private function _exists_track(int $id): int {
    return $this->_node_exists($id, 'song');
  }

  private function _node_exists(mixed $id, string $type, $field_name = 'field_discogs_id'): int {
    $query = Drupal::entityQuery('node')
      ->condition('type', $type)
      ->condition($field_name, $id);

    $node_ids = array_values($query->execute());

    if (count($node_ids) != 0) {
      return $node_ids[0];
    }
    else {
      return -1; // Item does not exists
    }
  }

  public function add_media_image(string $url, string $name, string $bundle): EntityInterface {
    $dir = 'sites/default/files/' . $bundle;
    Drupal::service('file_system')->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
    $file_data = file_get_contents($url);

    // https://stackoverflow.com/questions/25237100/how-to-get-mime-type-of-an-image-with-file-get-contents-in-php
    $pattern = "/^content-type\s*:\s*(.*)$/i";
    $content_type = NULL;
    if (($header = array_values(preg_grep($pattern, $http_response_header))) && (preg_match($pattern, $header[0], $match) !== false)) {
      $content_type = $match[1];
    }

    $content_type = explode('/', $content_type)[1];

    $url_parts = explode('/', $url);
    $file_name = end($url_parts);
    $file = file_save_data($file_data, 'public://' . $bundle . '/' . $file_name . '.' . $content_type);

    $media = Media::create([
      'bundle'=> $bundle,
      'uid' => \Drupal::currentUser()->id(),
      'field_media_image' => [
        'target_id' => $file->id(),
        'alt' => $name, // TODO: Maybe change to <Artist> - <Album name>
      ],
    ]);

    $media->setName($name)
      ->setPublished()
      ->save();

    return $media;
  }
}
