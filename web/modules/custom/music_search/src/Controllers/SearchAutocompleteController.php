<?php

namespace Drupal\music_search\Controllers;

use Drupal;
use Drupal\music_search_spotify\MusicSearchSpotifyService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchAutocompleteController {
  /**
   * @param Request $request
   * @return JsonResponse
   */
  public function handle_autocomplete(Request $request): JsonResponse {
    $results = [];
    $input = $request->query->get('q');

    if (!$input) {
      return new JsonResponse($results);
    }

    $search_result = Drupal::getContainer()->get(MusicSearchSpotifyService::class)->perform_search($input);

    foreach ($search_result->albums as $album) {
      if (!empty($album->id) && !empty($album->name)) {
        $results[] = [
          'value' => $album->name . ' (' . $album->id . ')',
          'label' => 'Album: ' . $album->name,
          'id' => $album->id,
          'type' => 'albums',
        ];
      }
    }

    foreach ($search_result->artists as $artist) {
      if (!empty($artist->id) && !empty($artist->name)) {
        $results[] = [
          'value' => $artist->name . ' (' . $artist->id . ')',
          'label' => 'Artist: ' . $artist->name,
          'id' => $artist->id,
          'type' => 'artists',
        ];
      }
    }

    foreach ($search_result->tracks as $track) {
      if (!empty($track->id) && !empty($track->name)) {
        $results[] = [
          'value' => $track->name . ' (' . $track->id . ')',
          'label' => 'Track: ' . $track->name,
          'id' => $track->id,
          'type' => 'tracks',
        ];
      }
    }

    Drupal::state()->set('autocomplete_result', $results);

    return new JsonResponse($results);
  }
}
